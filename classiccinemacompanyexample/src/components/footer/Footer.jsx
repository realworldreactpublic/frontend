import './footer.css';

import FooterLogo from './FooterLogo';
import FindUs from './FindUs';
import FollowUs from './FollowUs';
import MoreInfo from './MoreInfo';

const classes = {
    sixColumns: `col-lg-2`,
    fourColumns: `col-3`,
    threeColumns: `col-lg-4`,
    twoColumns: `col-lg-6`,
    oneColumn: `col-lg-12`,
    custom: `customFooter`
}

const Footer = () => (
    <footer>
        <div className="container">
            <div id="footer-content" className="row">
                <FooterLogo classes={classes} />
                <MoreInfo classes={classes} />
                <FindUs classes={classes} />
                <FollowUs classes={classes} />
            </div>
            <div className="row justify-content-center">
                <p id="copyright">Copyright &copy; The Classic Cinema Company Ltd 2020.<br />All rights reserved.</p>
            </div>
        </div>
    </footer>
);

export default Footer;
