import logo from '../../images/CCC-Logo2.png';
import PropTypes from 'prop-types';

const FooterLogo = ({ classes }) => {
    return (
        <div className={`${classes.sixColumns} ${classes.custom}`}>
            <img src={logo} alt="Classic Cinema Company Logo" className="footer-image" />
        </div>
    );
};

FooterLogo.propTypes = {
    classes: PropTypes.shape({
        sixColumns: PropTypes.string.isRequired,
        custom: PropTypes.string.isRequired
    }).isRequired
}

export default FooterLogo;
