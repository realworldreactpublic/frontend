import { lorem } from '../../../data/lorem.json';
import './signUp.css';

const SignUpContent = () => {

    return (
        <div className="row m-5" id="signUpContent">
            <div className="col" id="signUpText">
                <p>
                    {lorem.lorem50}
                </p>
            </div>
            <div className="col">
                <form id="signUpForm">
                    <div className="form-group form-row">
                        <label className="col-md-2 col-form-label" htmlFor="titleSelect">Title</label>
                        <div className="col-md-10">
                            <select className="form-control" id="titleSelect">
                                <option value="" disabled >Select an option</option>
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Miss">Miss</option>
                                <option value="Ms">Ms</option>
                            </select>
                        </div>
                    </div>
                    <div className="form-group form-row">
                        <label htmlFor="name" className="col-md-2 col-form-label" >Name</label>
                        <div className="col-lg-5 col-md-5">
                            <input className="form-control" placeholder="First Name" />
                        </div>
                        <div className="col-lg-5 col-md-5 stacked-control">
                            <input className="form-control" placeholder="Surname" />
                        </div>
                    </div>
                    <div className="form-group form-row">
                        <label htmlFor="email" className="col-md-2 col-form-label">Email</label>
                        <div className="col-md-10">
                            <input className="form-control" type="email" placeholder="yourname@yourdomain.com" id="email" />
                        </div>
                    </div>
                    <div className="form-group form-row">
                        <label htmlFor="phoneNo" className="col-md-2 col-form-label">Phone</label>
                        <div className="col-md-10">
                            <input type="tel" className="form-control" placeholder="01234567890" id="phoneNo" />
                        </div>
                    </div>
                    <div className="form-group form-row">
                        <label htmlFor="dob" className="col-md-2 col-form-label">DoB</label>
                        <div className="col-md-10">
                            <input className="form-control" type="date" placeholder="DD/MM/YYYY" id="dob" />
                        </div>
                    </div>
                    <div className="form-group form-row">
                        <label htmlFor="gender" className="col-lg-2 col-form-label">Gender</label>
                        <div className="form-row col-lg-6">
                            <div className="form-check form-check-inline custom-control custom-radio custom-control-inline">
                                <input type="radio" name="gender" value="female" id="femaleRadio" className="custom-control-input" />
                                <span className="checkmark"></span>
                                <label htmlFor="femaleRadio" className="custom-control-label ccc">Female</label>
                            </div>
                            <div className="form-check form-check-inline custom-control custom-radio custom-control-inline">
                                <input type="radio" name="gender" value="male" id="maleRadio" className="custom-control-input" />
                                <label htmlFor="maleRadio" className="custom-control-label ccc">Male</label>
                            </div>
                        </div>
                        <div className="col-lg-4" style={{ textAlign: `right` }}>
                            <input type="submit" value="Sign Up" id="btn-sign-up" />
                        </div>
                    </div>
                </form>
            </div >
        </div >
    );
}

export default SignUpContent;