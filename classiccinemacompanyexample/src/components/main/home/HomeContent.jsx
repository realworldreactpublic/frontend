import NowShowingContainer from "./nowshowing/NowShowingContainer";
import SiteIntro from "./siteintro/SiteIntro";

const HomeContent = () => {
    return (
        <>
            <SiteIntro />
            <NowShowingContainer />
        </>
    );
};

export default HomeContent;
