import { films } from '../../../../data/films.json';
import NowShowingCard from './NowShowingCard';

const NowShowingContainer = () => {

    const filmCards = films.map(film => <NowShowingCard key={film.id} film={film} />);

    return (
        <div className="container" style={{ marginBottom: `100px` }}>
            <h1>NOW SHOWING</h1>
            <div className="row">
                {filmCards}
            </div>
        </div>
    );
};

export default NowShowingContainer;
