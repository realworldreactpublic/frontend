import ImageSection from './ImageSection';
import TextSection from './TextSection';
import './siteIntro.css';

import { lorem } from '../../../../data/lorem.json';

const SiteIntro = () => {
    return (
        <div className="container" style={{ marginBottom: `50px` }}>
            <div className="row">
                <div className="col-md-6 left">
                    <TextSection contentText={lorem.lorem30} classes="top left" />
                    <ImageSection img={{ src: `home-snacks.png`, alt: `CCC Snacks` }} classes="middle left" />
                    <TextSection contentText={lorem.lorem50} classes="bottom left" />
                </div>
                <div className="col-md-6 right">
                    <TextSection contentText={lorem.lorem100} classes="top right" />
                    <ImageSection img={{ src: `home-tickets.png`, alt: `CCC Tickets` }} classes="bottom right" />
                </div>
            </div>
        </div>
    );
};

export default SiteIntro;
