const importImages = async (film, setAge, setSrcSet, path, srcSet) => {
    if (setAge) {
        const ageIcon = await import(`../../images/ageicons/${film.agecert}.png`);
        setAge(ageIcon.default);
    }

    const small = await import(`../../images/films/${film.shortname}/${path}/${film[srcSet].small}`);
    const medium = await import(`../../images/films/${film.shortname}/${path}/${film[srcSet].medium}`);
    const large = await import(`../../images/films/${film.shortname}/${path}/${film[srcSet].large}`);
    const xlarge = await import(`../../images/films/${film.shortname}/${path}/${film[srcSet].xlarge}`);
    setSrcSet(`${small.default} 200w, ${medium.default} 770w, ${large.default} 1140w, ${xlarge.default} 1400w`);
}

export default importImages;