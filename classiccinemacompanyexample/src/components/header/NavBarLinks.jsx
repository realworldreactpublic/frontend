import {NavLink} from 'react-router-dom';

const NavBarLinks = () => {
    return (
        <ul className="navbar-nav chevron">
            <li className="nav-item"><NavLink to="/whatsOn" activeClassName="active" className="nav-link">What's<br />On</NavLink></li>
            <li className="nav-item"><a href="#" className="nav-link">Coming<br />Soon</a></li>
            <li className="nav-item"><NavLink to="/signUp" activeClassName="active" className="nav-link">Sign<br />Up</NavLink></li>
            <li className="nav-item"><a href="#" className="nav-link">Your<br />Visit</a></li>
            <li className="nav-item"><a href="#" className="nav-link">Book<br />Tickets</a></li>
        </ul>
    );
};

export default NavBarLinks;
