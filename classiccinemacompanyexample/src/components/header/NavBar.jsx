import BurgerBtn from "./BurgerBtn";
import NavBarLinks from "./NavBarLinks";
import NavSearch from "./NavSearch";
import logo from '../../images/CCC-Logo.png';
import {Link} from 'react-router-dom';

const NavBar = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark">
            <div className="container-fluid">
                <Link to="/" className="navbar-brand">
                    <img src={logo} alt="Classic Cinema Company Logo" />
                </Link>
                <BurgerBtn />
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <NavBarLinks />
                </div>
                <NavSearch />
            </div>
        </nav>
    );
};

export default NavBar;
