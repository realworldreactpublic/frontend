import { render, act } from '@testing-library/react';
import App from '../App';

describe('App snapshot test', () => {

  test('should render the same snapshot each time', async () => {

    let appComponent = null;

    await act(async () => appComponent = await render(<App />));

    expect(appComponent).toMatchSnapshot();

  });

});
