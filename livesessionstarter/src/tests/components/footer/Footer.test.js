import { render, act } from '@testing-library/react';

import Footer from '../../../components/footer/Footer';

describe('Footer snapshot test', () => {

    test('should render the same snapshot each time', async () => {

        let footerComponent = null;

        await act(async () => footerComponent = await render(<Footer />));

        expect(footerComponent).toMatchSnapshot();

    });

});