import { render, act } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

import Header from '../../../components/header/Header';

describe('Header snapshot test', () => {

    test('should render the same snapshot each time', async () => {

        let headerComponent = null;

        await act(async () => headerComponent = await render(<MemoryRouter><Header /></MemoryRouter>));

        expect(headerComponent).toMatchSnapshot();

    });

});