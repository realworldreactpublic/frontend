import { act, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import WhatsOnContent from '../../../../components/main/whatsOn/WhatsOnContent';

import { films } from '../../../../data/films.json';

describe('WhatsOnContent rendering tests', () => {
    const push = jest.fn();

    test('should render 6 WhatsOnCard components with test data - which means 12 images (6 film, 6 certs)', async () => {
        await act(async () => await render(<WhatsOnContent history={{ push }} />));

        expect(screen.getAllByRole(/img/)).toHaveLength(12);
    });

});

describe('WhatsOnContent event firing tests', () => {

    const push = jest.fn();

    test('should call push when one of the WhatsOnCard images is clicked', async () => {
        const [{ id }] = films;
        await act(async () => await render(<WhatsOnContent history={{ push }} />));

        userEvent.click(screen.getAllByRole(/img/)[0]);

        expect(push).toHaveBeenCalledWith(`/film/${id}`);
    });
})

