import { render, act } from '@testing-library/react';

import SignUpContent from '../../../../components/main/signUp/SignUpContent';

describe('SignUpContent snapshot test', () => {

    test('should render the same snapshot each time', async () => {

        let signUpContentComponent = null;

        await act(async () => signUpContentComponent = await render(<SignUpContent />));

        expect(signUpContentComponent).toMatchSnapshot();

    });

});