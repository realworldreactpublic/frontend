import { render } from '@testing-library/react';

import YourVisit from '../../../../components/main/yourVisit/YourVisit';

describe('<YourVisit />', () => {

    test('should render the same snapshot each time', async () => {

        const yourVisitComponent = await render(<YourVisit />);

        expect(yourVisitComponent).toMatchSnapshot();

    });

});

