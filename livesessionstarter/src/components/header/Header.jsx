import { Route } from 'react-router-dom';
import './header.css';
import HeaderBannerImg from './HeaderBannerImg';
import NavBar from './NavBar';

const Header = () => {
    return (
        <header>
            <div className="container">
                <NavBar />
                <Route component={HeaderBannerImg}/>
            </div>
        </header>
    );
};

export default Header;
