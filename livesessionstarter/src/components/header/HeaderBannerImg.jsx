import homeBanner from '../../images/HomeBanner/HomeBanner_hmwvoh_c_scale,w_1400.png';
import PropTypes from 'prop-types';

const HeaderBannerImg = ({ location: { pathname } }) => {

    return (
        <img
            className="img-fluid"
            src={homeBanner}
            alt="Classic Cinema Company Header Banner"
        />
    );
};

HeaderBannerImg.propTypes = {
    location: PropTypes.shape({
        pathname: PropTypes.string.isRequired
    }).isRequired
}

export default HeaderBannerImg;
