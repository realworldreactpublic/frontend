import { useParams } from 'react-router-dom';
import { films } from '../../../data/films.json';
import { useState, useEffect } from 'react';
import './movieDetails.css';
import loadImages from '../loadImages';

const DetailsContent = () => {

    const { id } = useParams();

    const film = films.find(f => {
        return Number.parseInt(id) === f.id;
    });

    const [currentSrc, setCurrentSrc] = useState(``);
    const [srcSet, setSrcSet] = useState(``);
    const [age, setAge] = useState('');

    useEffect(() => {
        loadImages(film, setAge, setSrcSet, "whatson", "whatsOnSrcset");
    }, [film]);

    return (
        <div className="m-5">
            <div className="row">
                <div className="col detailsTitle">
                    <h1>{film.title.toUpperCase()}</h1>
                </div>
            </div>
            <div className="row">
                <div className="col-12 col-md-5">
                    <img
                        className="img-fluid poster"
                        src={currentSrc}
                        alt={`${film.title.toUpperCase()}`}
                        srcSet={srcSet}
                        onLoad={event => setCurrentSrc(event.target.currentSrc)}
                        style={{ objectFit: `cover`, minHeight: `90%` }}
                    />
                </div>
                <div className="col-md-5 col">
                    <div className="row">
                        <div className="col-3">
                            <p>Synopsis:</p>
                        </div>
                        <div className="col">
                            {film.synopsis}
                        </div>
                    </div>
                    <div className="row mt-2">
                        <div className="col-3">
                            <p>Rating:</p>
                        </div>
                        <div className="col">
                            <img src={age} alt={film.agecert.toUpperCase()} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default DetailsContent;