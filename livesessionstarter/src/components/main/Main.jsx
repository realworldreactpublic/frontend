import HomeContent from './home/HomeContent';
import { Route, Switch } from 'react-router-dom';
import WhatsOnContent from './whatsOn/WhatsOnContent';
import SignUpContent from './signUp/SignUpContent';
import DetailsContent from './movieDetails/DetailsContent';
import YourVisit from './yourVisit/YourVisit';

const Main = () => {
    return (
        <main className="container">
            <Switch>
                <Route exact path="/" component={HomeContent} />
                <Route path="/film/:id" component={DetailsContent} />
                <Route path="/whatsOn" component={WhatsOnContent} />
                <Route path="/signUp" component={SignUpContent} />
                <Route path="/yourVisit" component={YourVisit} />
            </Switch>
        </main>
    );
};

export default Main;
