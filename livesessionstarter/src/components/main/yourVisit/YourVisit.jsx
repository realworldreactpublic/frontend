import { openingTimes } from '../../../data/openingTimes.json';

const YourVisit = () => {

    const openingTimesDisplay = openingTimes.map((openingTime, index) => (
        <div className="row" key={`${openingTime.day}${index}`}>
            <div className="col-4">
                {openingTime.day}
            </div>
            <div className="col-4">
                {openingTime.open}
            </div>
            <div className="col-4">
                {openingTime.close}
            </div>
        </div>
    ));

    return (
        <div className="container">
            <div className="row">
                <h3>When we're open:</h3>
            </div>
            <div className="row">
                <div className="col-4 offset-4">
                    Open
                </div>
                <div className="col-4">
                    Close
                </div>
            </div>
            {openingTimesDisplay}
        </div>
    );
};

export default YourVisit;
