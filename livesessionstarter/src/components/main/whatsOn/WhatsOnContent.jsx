import { films } from '../../../data/films.json';
import WhatsOnCard from './WhatsOnCard';
import PropTypes from 'prop-types';

const WhatsOnContent = ({ history: { push } }) => {

    const handleClick = (film) => push(`/film/${film.id}`);
    const filmCards = films.map(film => <WhatsOnCard clickHandler={handleClick} film={film} key={film.id} />);

    return (
        <div className="row">
            {filmCards}
        </div>
    );
};

WhatsOnContent.propTypes = {
    history: PropTypes.shape({
        push: PropTypes.func.isRequired
    }).isRequired
}

export default WhatsOnContent;
