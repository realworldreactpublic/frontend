import { useState, useEffect } from 'react';
import './whatsOnCard.css';
import PropTypes from 'prop-types';
import loadImages from '../loadImages';

const WhatsOnCard = ({ clickHandler, film }) => {

    const [currentSrc, setCurrentSrc] = useState(``);
    const [srcSet, setSrcSet] = useState(``);
    const [age, setAge] = useState('');

    useEffect(() => {
        loadImages(film, setAge, setSrcSet, "whatson", "whatsOnSrcset");
    }, [film]);

    const times = film.showtimes.map((time, i) => {
        return (
            <div className="col-2" key={`time${i}`}>
                <p >{time}</p>
            </div>
        );
    });

    return (
        <div className="col-lg-6 mt-5">
            <div className="container whatsOnCard">
                <div className="row">
                    <div className="col">
                        <img
                            className="img-fluid poster"
                            src={currentSrc}
                            alt={`${film.title.toUpperCase()}`}
                            srcSet={srcSet}
                            onLoad={event => setCurrentSrc(event.target.currentSrc)}
                            style={{ objectFit: `cover`, minHeight: `90%` }}
                            onClick={() => clickHandler(film)}
                        />
                    </div>
                    <div className="col">
                        <div className="row">
                            <h4 className="whatsOnTitle" onClick={() => clickHandler(film)}>{film.title.toUpperCase()}<img src={age} alt={film.agecert.toUpperCase()} /></h4>
                        </div>
                        <div className="row">
                            <p>
                                {film.synopsis}
                            </p>
                        </div>
                    </div>
                </div>
                <div className="row mt-2">
                    {times}
                    <div className="col-6">
                        <h5>Book now</h5>
                    </div>
                </div>
            </div>


        </div>
    );
};

WhatsOnCard.propTypes = {
    film: PropTypes.exact({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        shortname: PropTypes.string.isRequired,
        synopsis: PropTypes.string.isRequired,
        showtimes: PropTypes.arrayOf(PropTypes.string).isRequired,
        agecert: PropTypes.string.isRequired,
        nowShowingSrcset: PropTypes.exact({
            small: PropTypes.string.isRequired,
            medium: PropTypes.string.isRequired,
            large: PropTypes.string.isRequired,
            xlarge: PropTypes.string.isRequired
        }).isRequired,
        whatsOnSrcset: PropTypes.exact({
            small: PropTypes.string.isRequired,
            medium: PropTypes.string.isRequired,
            large: PropTypes.string.isRequired,
            xlarge: PropTypes.string.isRequired
        }).isRequired
    }).isRequired,
    clickHandler: PropTypes.func.isRequired
}

export default WhatsOnCard;
