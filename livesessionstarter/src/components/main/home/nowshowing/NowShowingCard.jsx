import { useState, useEffect } from 'react';
import './nowShowingCard.css';
import PropTypes from 'prop-types';
import importImages from '../../loadImages';

const NowShowingCard = ({ film }) => {

    const [currentSrc, setCurrentSrc] = useState(``);
    const [srcSet, setSrcSet] = useState(``);

    useEffect(() => {
        importImages(film, null, setSrcSet, "nowshowing", "nowShowingSrcset");
    }, [film]);

    return (
        <div className="col-lg-4 col-md-6 now-showing-card">
            <img
                className="img-fluid"
                src={currentSrc}
                alt={`${film.title.toUpperCase()}`}
                srcSet={srcSet}
                onLoad={event => setCurrentSrc(event.target.currentSrc)}
                style={{ objectFit: `cover`, minHeight: `90%` }}
            />
            <h4>{film.title.toUpperCase()}</h4>
        </div>
    );
};

NowShowingCard.propTypes = {
    film: PropTypes.exact({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        shortname: PropTypes.string.isRequired,
        synopsis: PropTypes.string.isRequired,
        showtimes: PropTypes.arrayOf(PropTypes.string).isRequired,
        agecert: PropTypes.string.isRequired,
        nowShowingSrcset: PropTypes.exact({
            small: PropTypes.string.isRequired,
            medium: PropTypes.string.isRequired,
            large: PropTypes.string.isRequired,
            xlarge: PropTypes.string.isRequired
        }).isRequired,
        whatsOnSrcset: PropTypes.exact({
            small: PropTypes.string.isRequired,
            medium: PropTypes.string.isRequired,
            large: PropTypes.string.isRequired,
            xlarge: PropTypes.string.isRequired
        })
    }).isRequired
}

export default NowShowingCard;
