import PropTypes from 'prop-types';

const TextSection = ({ contentText, classes }) => {
    return (
        <p className={classes}>
            {contentText}
        </p>
    );
};

TextSection.propTypes = {
    contentText: PropTypes.string.isRequired,
    classes: PropTypes.string.isRequired
}

export default TextSection;
