import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const ImageSection = ({ img, classes }) => {

    const [imageSrc, setImageSrc] = useState(``);

    useEffect(() => {

        const getImage = async () => {
            const image = await import(`../../../../images/${img.src}`);
            setImageSrc(image.default);
        };

        getImage();
    }, [img]);

    return (
        <>
            {img &&
                <img src={imageSrc} width="100%" className={classes} alt={img.alt} />}
        </>
    );
};

ImageSection.propTypes = {
    img: PropTypes.shape({
        src: PropTypes.string.isRequired,
        alt: PropTypes.string.isRequired
    }),
    classes: PropTypes.string.isRequired
}

export default ImageSection;
