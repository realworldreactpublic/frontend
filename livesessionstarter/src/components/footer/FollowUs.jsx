import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF, faInstagram, faTwitter, faYoutube } from '@fortawesome/free-brands-svg-icons';
import PropTypes from 'prop-types';

const FollowUs = ({ classes }) => {
    return (
        <div className={`${classes.threeColumns} ${classes.custom}`}>
            <div className="row">
                <div className={classes.oneColumn}>
                    <h6>FOLLOW US</h6>
                </div>
            </div>
            <div className="row">
                <div className={classes.fourColumns}>
                    <FontAwesomeIcon icon={faTwitter} size="3x" className="fa-icon" />
                </div>
                <div className={classes.fourColumns}>
                    <FontAwesomeIcon icon={faInstagram} size="3x" className="fa-icon" />
                </div>
                <div className={classes.fourColumns}>
                    <FontAwesomeIcon icon={faFacebookF} size="3x" className="fa-icon" />
                </div>
                <div className={classes.fourColumns}>
                    <FontAwesomeIcon icon={faYoutube} size="3x" className="fa-icon" />
                </div>
            </div>
        </div>
    );
};

FollowUs.propTypes = {
    classes: PropTypes.shape({
        oneColumn: PropTypes.string.isRequired,
        threeColumns: PropTypes.string.isRequired,
        fourColumns: PropTypes.string.isRequired,
        sixColumns: PropTypes.string.isRequired,
        custom: PropTypes.string.isRequired
    }).isRequired
}

export default FollowUs;
