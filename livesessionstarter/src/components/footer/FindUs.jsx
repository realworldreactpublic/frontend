import React from 'react';
import PropTypes from 'prop-types';
import map from '../../images/map.png';

const FindUs = ({ classes }) => {
    return (
        <div className={`${classes.threeColumns} ${classes.custom}`}>
            <div className="row">
                <div className={classes.oneColumn}>
                    <h6>FIND US</h6>
                </div>
            </div>
            <div className="row align-items-center">
                <div className={classes.twoColumns}>
                    <img src={map} alt="Classic Cinema Company Location Map Thumbnail" className="footer-image" />
                </div>
                <div className={classes.twoColumns}>
                    <p id="findus">Lorem ipsum, dolor sit amet consectetur</p>
                </div>
            </div>
        </div>
    );
};

FindUs.propTypes = {
    classes: PropTypes.shape({
        oneColumn: PropTypes.string.isRequired,
        twoColumns: PropTypes.string.isRequired,
        threeColumns: PropTypes.string.isRequired,
        sixColumns: PropTypes.string.isRequired,
        custom: PropTypes.string.isRequired
    }).isRequired
}

export default FindUs;
