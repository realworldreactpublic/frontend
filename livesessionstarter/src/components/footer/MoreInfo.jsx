import PropTypes from 'prop-types';

const MoreInfo = ({ classes }) => {
    return (
        <div className={`${classes.sixColumns} ${classes.custom}`}>
            <div className="row">
                <div className={classes.oneColumn}>
                    <h6>MORE INFO</h6>
                </div>
            </div>
            <div className="row align-items-center" style={{ minHeight: `100px` }}>
                <div className={classes.oneColumn}>
                    <nav>
                        <ul className="nav flex-column">
                            <li className="nav-item footer-link"><a href="/aboutus">About Us</a></li>
                            <li className="nav-item  footer-link"><a href="/faq">FAQ</a></li>
                            <li className="nav-item  footer-link"><a href="/contactus">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    );
};

MoreInfo.propTypes = {
    classes: PropTypes.shape({
        oneColumn: PropTypes.string.isRequired,
        sixColumns: PropTypes.string.isRequired,
        custom: PropTypes.string.isRequired
    }).isRequired
}

export default MoreInfo;
